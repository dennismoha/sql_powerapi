- This file explains the goal of the sheet.
- This is the file source: [ https://www.mysqltutorial.org/mysql-sample-database.aspx] and it's model is  in the image folder 

- The goal of this sheet is to connect mysql to excel , 
    ** Fetch mysql data from a given database  / table to excel
    ** manipulate the data : Sort data, filter, 
    ** update data once an excel sheet is changed
    ** Refresh data to the sheet incase data is changed in the mysql database table

- I used MYSQL EXCEL ODBC ANSI drivers for 32/64 bits windows System to make connectivity between excel and mysql possible.

- Steps:
    1) in sheet 1 get  Employee details in Microsoft Query
    2) Add the offices table in the same query 
    3)  link them with their office codes in respect to the foreign key