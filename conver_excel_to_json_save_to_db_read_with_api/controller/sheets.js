const xlsx = require('xlsx');
const fs = require('fs')
var path = require('path');
const db = require("../db/config");
const Axios = require('axios')

const workbook = path.join(__dirname, '/Book3.xlsx'); //path to the workbook
const arrays = []
const values = []


const getData =  (req, res)=>{
    var workbooks =xlsx.readFile(workbook)
    const workSheet = workbooks.SheetNames  // get all the work sheet names
    const workSheetData = workbooks.Sheets['employees']    //upload excel file
    // read data
    // save to db
    // get it via api
    
   
    const worksheetArray = xlsx.utils.sheet_to_json( workSheetData)
   
    db.query('INSERT INTO employees (email, employeeNumber, extension, firstName, jobTitle, lastName, officeCode) values ?'
    ,[worksheetArray.map(arrays=>[arrays.email,arrays.employeeNumber, arrays.extension,arrays.firstName,arrays.jobTitle,arrays.lastName,arrays.officeCode])])
    .then(([results])=>{
       return res.status(200).json(results)
    })
    .catch((errors)=>{
        console.log(errors)
        return res.status(400).json({message:"error happened"})
    })  
  

}

const passDataToApi = (req, res) =>{
    // url: https://jsonplaceholder.typicode.com/guide/
    var workbooks =xlsx.readFile(workbook)
    const workSheet = workbooks.SheetNames  // get all the work sheet names
    const workSheetData = workbooks.Sheets['Sheet1']
    const worksheetArray = xlsx.utils.sheet_to_json( workSheetData)
    console.log(workSheet)  
    const data = worksheetArray[0]
    const data2 = JSON.stringify(data)
    Axios.post("https://jsonplaceholder.typicode.com/posts",{
        body:data,
        headers:{
            'Content-type': 'application/json; charset=UTF-8',
        }
    })
    .then((response) => {
     res.status(200).json(response.data.body)})
    .catch((error) => console.log(error));

    

}

module.exports={getData,passDataToApi}