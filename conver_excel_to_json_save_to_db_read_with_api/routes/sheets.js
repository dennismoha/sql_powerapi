var express = require('express');
const Sheets = require('../controller/sheets')
var router = express.Router();

/* GET users listing. */
router.get('/', Sheets.getData)
router.get('/passDataToApi', Sheets.passDataToApi)
router.get('/readme',(req,res)=>{
    res.render('readme')
})

module.exports = router;
